import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAuth } from 'src/app/models/auth.model';
import { AuthActions } from 'src/app/store/auth/auth.actions';
import { AuthStateService } from 'src/app/store/auth/auth.state.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  userName = 'user';
  password = 'password';
  constructor(
    private router: Router,
    private authStateService: AuthStateService
  ) {}

  ngOnInit(): void {}

  login() {

 
    const payload: IAuth = {
      username: this.userName,
     
    };
    this.authStateService.authDispatcher.next({
      type: AuthActions.LOGIN,
      payload,
    });
    this.router.navigate(['/feed'])
  }
}
