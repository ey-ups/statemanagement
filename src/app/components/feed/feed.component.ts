import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAuth } from 'src/app/models/auth.model';
import { AuthActions } from 'src/app/store/auth/auth.actions';
import { AuthStateService } from 'src/app/store/auth/auth.state.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css'],
})
export class FeedComponent implements OnInit {
  username: string;
  loginStatus :string;
  constructor(
    private authStateService: AuthStateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authStateService.authStore.subscribe(value=>{
      this.username = value.username;
      console.log("value,",this.username)
      console.log("value,",value)
    })
    }

  logout() {
    this.router.navigate(['/login']);
  }
}
