import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICourse } from 'src/app/models/auth.model';
import { CourseActions } from 'src/app/store/course/course.actions';
import { CourseStateService } from 'src/app/store/course/course.state.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
})
export class CourseComponent implements OnInit {
  courseName = 'kurs';
  courseCategory = 'category';
  courses: ICourse[] = [];
  constructor(
    private courseStateService: CourseStateService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.courseStateService.courseStore.subscribe((value) => {
      this.courses = value;
    });
  }
  add() {
    const payload: ICourse = {
      name: this.courseName,
      category: this.courseCategory,
    };
    this.courseStateService.courseDispatcher.next({
      type: CourseActions.ADD,
      payload,
    });
  }

  clear() {
    this.courseStateService.courseDispatcher.next({
      type:CourseActions.CLEAR,
    })
  }
}
