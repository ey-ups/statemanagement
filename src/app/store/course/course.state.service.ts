import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { IAction, ICourse } from 'src/app/models/auth.model';
import { CourseActions } from './course.actions';

@Injectable({
  providedIn: 'root',
})
export class CourseStateService {
  courseStore: BehaviorSubject<ICourse[]>;
  courseDispatcher: Subject<IAction>;

  constructor() {

    this.courseStore = new BehaviorSubject<any>([]);
    this.courseDispatcher = new Subject<IAction>();

    this.courseDispatcher.subscribe((action: IAction) => {
      

      switch (action.type) {
        case CourseActions.ADD:
          console.log("LOGİN PAYLOAD");
          this.courseStore.next([...this.courseStore.getValue(),action.payload]);
          console.log("bu", this.courseStore)
          break;

        case CourseActions.CLEAR:
          console.log('CLEAR');
          this.courseStore.next([]);
          break;
      }
    });
  }

}
