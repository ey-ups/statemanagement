import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { IAction, IAuth } from 'src/app/models/auth.model';
import { AuthActions } from './auth.actions';

@Injectable({
  providedIn: 'root',
})
export class AuthStateService {
  authStore: BehaviorSubject<IAuth>;
  authDispatcher: Subject<IAction>;

  constructor() {

    this.authStore = new BehaviorSubject<any>([]);
    this.authDispatcher = new Subject<IAction>();

    this.authDispatcher.subscribe((action: IAction) => {
      

      switch (action.type) {
        case AuthActions.LOGIN:
          console.log("LOGİN PAYLOAD");
          this.authStore.next(action.payload);
          break;

        case AuthActions.LOGOUT:
          console.log('logut');
          break;
      }
    });
  }
 
  
}
