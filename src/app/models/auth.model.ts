export interface IAuth {
    username: string;
}
export interface ICourse {
    name: string;
    category: string;
}

export interface IAction{
    type: string;
    payload?: any;
}

